/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Dialog {
    id: renderUri

    property var uriJson
    property string authType: keyPeriod.visible
        ? "TOTP"
        : "HTOP"
    title: i18n.tr("Add Authenticator Key")
    text: i18n.tr("%1 authenticator key").arg(authType)

    TextField {
        id: userDdescription
        placeholderText: i18n.tr("Add a description")
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    Text {
        text: "<b>" + i18n.tr("Issuer") + ":</b> " + uriJson.issuer
        color: theme.palette.normal.overlayText
    }

    Text {
        text: "<b>" + i18n.tr("User") + ":</b> " + uriJson.label
        color: theme.palette.normal.overlayText
    }

    Text {
        text: "<b>" + i18n.tr("Algorithm") + ":</b> " + uriJson.algorithm
        color: theme.palette.normal.overlayText
    }

    Row {
        spacing: units.gu(2)

        Text {
            text: "<b>" + i18n.tr("Password Length") + ":</b> " + uriJson.digits
            color: theme.palette.normal.overlayText
        }

        Text {
            id: keyPeriod
            visible: uriJson.counter == undefined
            text: "<b>" + i18n.tr("Interval") + ":</b> " + uriJson.period
            color: theme.palette.normal.overlayText
        }

        Text {
            visible: !keyPeriod.visible
            text: "<b>" + i18n.tr("Counter") + ":</b> " + uriJson.counter
            color: theme.palette.normal.overlayText
        }
    }

    Text {
        text: "<b>" + i18n.tr("Key") + ":</b> " + uriJson.secret.b32
        color: theme.palette.normal.overlayText
        wrapMode: Text.WrapAnywhere
    }

    Button {
        text: i18n.tr("Add")
        color: theme.palette.normal.positive
        onClicked: addKey()
    }

    Button {
        text: i18n.tr("Cancel")
        onClicked: closingPop()
    }

    function addKey() {
        KeysDB.storeKey( Date(), uriToRender, userDdescription.text);
        root.initDB();
        renderUri.hide();
    }

    function closingPop() {
        renderUri.hide();
    }

    Component.onCompleted: {
        uriJson = OTPAuth.URI.parse(root.uriToRender);
        show();
    }

    Component.onDestruction: {
        //To be used when PopupBase is closed
    }

}
