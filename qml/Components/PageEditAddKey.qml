/*
 */

import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import "../js/db.js" as KeysDB

Page {
    id: editAddKeyPage

    property var uriToEdit
    property alias descriptionToEdit: info.userDesc
    property bool editMode: false
    property int idToUpdate

    header: HeaderBase {
        id: pageHeader
        title: i18n.tr("Key Information")

        trailingActionBar {
        actions: Action {
                iconName: "ok"
                text: editMode
                    ? i18n.tr("Edit")
                    : i18n.tr("Add")

                enabled: !info.isInfoEmpty
                onTriggered: addKey()
            }
        }
    }

    Flickable {
        id: editAddFlickable
        width: parent.width
        height: parent.height
        clip: true
        contentHeight: info.height
        topMargin: pageHeader.height + units.gu(2)
        bottomMargin: Qt.inputMethod.visible && !editMode
            ? Qt.inputMethod.keyboardRectangle.height + units.gu(2)
            : units.gu(4)

        KeyToEdit {
            id: info
            width: parent.width * .85
            anchors.horizontalCenter: parent.horizontalCenter
            editMode: editAddKeyPage.editMode
        }
    }

    function addKey() {
        var editedKey = info.checkFields();

        if (info.isTOTP) {
            editedKey = new OTPAuth.TOTP(editedKey);
        } else {
            editedKey = new OTPAuth.HOTP(editedKey);
        }

        if (editMode) {
            KeysDB.updateStoredKey(idToUpdate, Date(), editedKey.toString(), info.userDesc);
        } else {
            KeysDB.storeKey(Date(), editedKey.toString(), info.userDesc);
        }

        root.initDB();
        closingPop();
    }

    function closingPop() {
        if (editMode) {
            mainStack.pop();
        } else {
            bottomEdge.collapse();
        }
    }

    Component.onCompleted: {
        if (editMode) {
            try {
                info.isTOTP = uriToEdit.match("totp") !== null;
                info.uriJson = OTPAuth.URI.parse(uriToEdit);
            } catch(e) {
                console.log("Error",e);
            }
        }
    }
}
