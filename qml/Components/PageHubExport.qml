import QtQuick 2.9
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Page {
    id: exportPage
    objectName: "exportHubPage"
    anchors.fill: parent

    property var activeTransfer
    property var path

    signal close()
    signal deleteCacheFile()

    header: HeaderBase {
        title: i18n.tr("Export json file to")
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: exportPage.header.height

        ContentTransferHint {
            anchors.fill: parent
            activeTransfer: activeTransfer
        }

        ContentItem {
            id: exportItem
        }

        ContentPeerPicker {
            visible: exportPage.visible
            showTitle: false
            handler: ContentHandler.Destination
            contentType: ContentType.Documents

            onPeerSelected: {
                activeTransfer = peer.request();
                stateChangeConnection.target = activeTransfer
                console.log("APP",activeTransfer.destination)
                console.log("Store",activeTransfer.store)
                console.log("DownID",activeTransfer.downloadId)
                var items = [];
                exportItem.url = path;
                console.log("1",JSON.stringify(exportItem))
                console.log("Export path:",path);
                items.push(exportItem);
                console.log("2",JSON.stringify(items))
                activeTransfer.items = items;
                activeTransfer.state = ContentTransfer.Charged;
            }

            onCancelPressed: {
                close();
            }
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                  console.log("Aborted")
                case ContentTransfer.Collected:
                  console.log("Finalize and close")
                  activeTransfer.finalize();
                  deleteCacheFile();
                  close();
                  break;
                case ContentTransfer.Finalized:
                  console.log("Finalized")
                  break;
                case ContentTransfer.InProgress:
                  console.log("InProgress")
                  break;
                case ContentTransfer.Charged:
                  break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }
}
